

int motor[2][4] = {
  {2, 3, 4, 5},
  {8, 9, 10, 11}
};

int steps[2] = {0, 0};

int INFERIOR = 1;
int SUPERIOR = 0;

int altura = 10;
int anchura = 6;





/*
int STEPS[4][4] = {
  {HIGH, HIGH, LOW, LOW},
  {LOW, HIGH, HIGH, LOW},
  {LOW, LOW, HIGH, HIGH},
  {HIGH, LOW, LOW, HIGH}
};*/

int MAX_STEP = 8;


/*
int STEPS[4][4] = {
  {HIGH, LOW, LOW, LOW},
  {LOW, HIGH, LOW, LOW},
  {LOW, LOW, HIGH, LOW},
  {LOW, LOW, LOW, HIGH}
};
*/


int STEPS[8][4] = {
  {HIGH, LOW, LOW, LOW},
  {HIGH, HIGH, LOW, LOW},
  {LOW, HIGH, LOW, LOW},
  {LOW, HIGH, HIGH, LOW},
  {LOW, LOW, HIGH, LOW},
  {LOW, LOW, HIGH, HIGH},
  {LOW, LOW, LOW, HIGH},
  {HIGH, LOW, LOW, HIGH}
};

void girarSentidoA(int index) {
  
      digitalWrite(motor[index][0], STEPS[steps[index]][0]);
      digitalWrite(motor[index][1], STEPS[steps[index]][1]);
      digitalWrite(motor[index][2], STEPS[steps[index]][2]);
      digitalWrite(motor[index][3], STEPS[steps[index]][3]);
    
    steps[index]++;
    if (steps[index] == MAX_STEP) steps[index] = 0;
}

void girarSentidoB(int index) {
  
      digitalWrite(motor[index][0], STEPS[steps[index]][0]);
      digitalWrite(motor[index][1], STEPS[steps[index]][1]);
      digitalWrite(motor[index][2], STEPS[steps[index]][2]);
      digitalWrite(motor[index][3], STEPS[steps[index]][3]);
    
    steps[index]--;
    if (steps[index] == -1) steps[index] = MAX_STEP - 1;
}


void subir() {
    girarSentidoB(SUPERIOR);
}

void bajar() {
    girarSentidoA(SUPERIOR);
}

void derecha() {
    girarSentidoB(INFERIOR);
}

void izquierda() {
    girarSentidoA(INFERIOR);
}




int laserpin = 13;


int readInt() {
    while (!Serial.available());
    byte a = Serial.read();
    if (a == '-') {
          while (!Serial.available());
          return -(Serial.read() - '0');
    }
    return a - '0';
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);  
  
  pinMode(laserpin, OUTPUT);
  digitalWrite(laserpin, HIGH);
  
  for (int m = 0; m < 2; m++)
    for (int i = 0; i < 4; i++) {
     pinMode(motor[m][i], OUTPUT);
     digitalWrite(motor[m][i], LOW); 
    }  
  
  
    Serial.println("Configura el motor inferior:");
    
    int val;
    
    do {
      while (Serial.available() < 2);
      val = Serial.parseInt();
      
      if (val < 0)
        for (int i = 0; i < -val; i++) {
          girarSentidoA(INFERIOR);
          delay(2);
        }
      else if (val > 0)
        for (int i = 0; i < val; i++) {
          girarSentidoB(INFERIOR);
          delay(2);
        }
    } while (val != 0);
    
    Serial.println("Configura el motor superior:");
    do {
      while (Serial.available() < 2);
      val = Serial.parseInt();
      
      if (val < 0)
        for (int i = 0; i < -val; i++) {
          girarSentidoA(SUPERIOR);
          delay(2);
        }
      else if (val > 0)
        for (int i = 0; i < val; i++) {
          girarSentidoB(SUPERIOR);
          delay(2);
        }
    } while (val != 0);
  
  
}


int retardo = 2;
int unidad = 8;

void dibujarCuadrado1() {
    for (int i = 0; i < unidad; i++) {
      subir();
      delay(retardo);
    }
  
    for (int i = 0; i < unidad; i++) {
      derecha();
      delay(retardo);
    }
    
    for (int i = 0; i < unidad; i++) {
      bajar();
      delay(retardo);
    }
    
    for (int i = 0; i < unidad; i++) {
      izquierda();
      delay(retardo);
    }
  
}

void dibujarCuadrado() {
    digitalWrite(laserpin,LOW);
    
    for (int i = 0; i < anchura/2; i++) {
      izquierda();
      delay(retardo);
    }
    
    digitalWrite(laserpin,HIGH);
  
    for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    
    for (int i = 0; i < anchura; i++) {
      derecha();
      delay(retardo);
    }
    
    for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
    }
    
    for (int i = 0; i < anchura; i++) {
      izquierda();
      delay(retardo);
    }
    
    for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    
    digitalWrite(laserpin,LOW);
    
    for (int i = 0; i < anchura/2; i++) {
      derecha();
      delay(retardo);
    }
    
    digitalWrite(laserpin,HIGH);
  
}



void dibujarTriangulo() {
  
      for (int i = 0; i < unidad; i++) {
      girarSentidoA(INFERIOR);
      if (i == unidad - 2)
        delay(retardo*3);
      else if (i == unidad - 1)
        delay(retardo*6);
      else
        delay(retardo);
    }
    
    for (int i = 0; i < unidad; i++) {
      if (i % 2 == 0) {
        girarSentidoB(INFERIOR);
        girarSentidoB(SUPERIOR);
      } else {
        girarSentidoB(INFERIOR);
        girarSentidoB(SUPERIOR);
      }
      if (i == unidad - 2)
        delay(retardo*3);
      else if (i == unidad - 1)
        delay(retardo*6);
      else
        delay(retardo);
    }

    for (int i = 0; i < unidad; i++) {
      if (i % 2 == 0) {
        girarSentidoA(SUPERIOR);
        girarSentidoB(INFERIOR);
      } else {
        girarSentidoA(SUPERIOR);
        girarSentidoB(INFERIOR);
      }
      if (i == unidad - 2)
        delay(retardo*3);
      else if (i == unidad - 1)
        delay(retardo*6);
      else
        delay(retardo);
    }
    
    for (int i = 0; i < unidad; i++) {
      girarSentidoA(INFERIOR);
      if (i == unidad - 2)
        delay(retardo*3);
      else if (i == unidad - 1)
        delay(retardo*6);
      else
        delay(retardo);
    }
}

void dibujarH() {
    for (int i = 0; i < anchura/2; i++) {
      izquierda();
      delay(retardo);
    }
    
   for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    
   for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
    }
    
    for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }

    for (int i = 0; i < anchura; i++) {
       derecha();
      delay(retardo);
    }
    
    for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    
    for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
    }
    
    for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
  
   for (int i = 0; i < anchura/2; i++) {
      izquierda();
      delay(retardo);
    }
}

void dibujarE() {
  for (int i = 0; i < anchura; i++) {
      derecha();
      delay(retardo);
  }
  for (int i = 0; i < anchura; i++) {
      izquierda();
      delay(retardo);
  }
  for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
  }
  for (int i = 0; i < anchura; i++) {
      derecha();
      delay(retardo);
  }
  for (int i = 0; i < anchura; i++) {
      izquierda();
      delay(retardo);
  }
  for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
  }
  for (int i = 0; i < anchura; i++) {
      derecha();
      delay(retardo);
  }
  for (int i = 0; i < anchura; i++) {
      izquierda();
      delay(retardo);
  }
  for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
  }
}

void dibujarM() {
  for (int i = 0; i < altura/2; i++) {
      subir();
      izquierda();
      delay(retardo);
  }
  for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
  }
  for (int i = 0; i < altura; i++) {
      subir();
      delay(retardo);
  }
  for (int i = 0; i < altura/2; i++) {
      bajar();
      derecha();
      delay(retardo);
  }
  for (int i = 0; i < altura/2; i++) {
      subir();
      derecha();
      delay(retardo);
  }
  for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
  }
  for (int i = 0; i < altura; i++) {
      subir();
      delay(retardo);
  }
  for (int i = 0; i < altura/2; i++) {
      bajar();
      izquierda();
      delay(retardo);
  }
}

void dibujarBarra() {
  for (int i = 0; i < altura/2; i++) {
      subir();
      subir();
      derecha();
      delay(retardo);
  }
  for (int i = 0; i < altura; i++) {
      bajar();
      bajar();
      izquierda();
      delay(retardo);
  }
  for (int i = 0; i < altura/2; i++) {
      subir();
      subir();
      derecha();
      delay(retardo);
  }
}

void dibujarJ() {
    for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
      izquierda();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
      derecha();
      delay(retardo);
    }
    for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
      izquierda();
      delay(retardo);
    }
    for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
      derecha();
      delay(retardo);
    }
    for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    
}

void dibujarP() {
   for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       derecha();
      delay(retardo);
    }
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       izquierda();
      delay(retardo);
    }
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
    }
   for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }

}

void dibujarG() {
    for (int i = 0; i < anchura/2; i++) {
       derecha();
      delay(retardo);
    }
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       izquierda();
      delay(retardo);
    }
   for (int i = 0; i < altura; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       derecha();
      delay(retardo);
    }


   digitalWrite(laserpin,LOW);
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
    }
    digitalWrite(laserpin,HIGH);
    for (int i = 0; i < anchura/2; i++) {
       izquierda();
      delay(retardo);
    }




}


void dibujarLOGO() {
  int anch = 8;
  int alt = 12;
   for (int i = 0; i < alt/2; i++) {
      bajar();
      delay(retardo);
    }
    digitalWrite(laserpin,LOW);
    for (int i = 0; i < anch/2; i++) {
       izquierda();
      delay(retardo);
    }
    digitalWrite(laserpin,HIGH);
   for (int i = 0; i < alt; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anch; i++) {
       derecha();
      delay(retardo);
    }
    digitalWrite(laserpin,LOW);
   for (int i = 0; i < alt/2; i++) {
      bajar();
      delay(retardo);
   }
   for (int i = 0; i < anch/2; i++) {
      izquierda();
      delay(retardo);
   }
   digitalWrite(laserpin,HIGH);

}

void dibujarLOGO1() {
  int anch = 8;
  int alt = 10;
   digitalWrite(laserpin,LOW);
   for (int i = 0; i < anchura/2; i++) {
      izquierda();
      delay(retardo);
    }
    digitalWrite(laserpin,HIGH);
    for (int i = 0; i < altura/2; i++) {
       bajar();
      delay(retardo);
    }
   for (int i = 0; i < altura; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       derecha();
      delay(retardo);
    }
    digitalWrite(laserpin,LOW);
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
   }
   digitalWrite(laserpin,HIGH);
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
   }
   for (int i = 0; i < altura/2; i++) {
     subir();
     delay(retardo);
   }
   digitalWrite(laserpin,LOW);
   for (int i = 0; i < anchura/2; i++) {
     izquierda();
     delay(retardo);
   }
   digitalWrite(laserpin,HIGH);
   

}



void dibujarI2() {
   for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }

    for (int i = 0; i < anchura/2; i++) {
       izquierda();
      delay(retardo);
    }
    
    for (int i = 0; i < anchura; i++) {
       derecha();
      delay(retardo);
    }
    for (int i = 0; i < anchura/2; i++) {
       izquierda();
      delay(retardo);
    }







   for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
    }

    for (int i = 0; i < anchura/2; i++) {
       izquierda();
      delay(retardo);
    }
    
    for (int i = 0; i < anchura; i++) {
       derecha();
      delay(retardo);
    }
    for (int i = 0; i < anchura/2; i++) {
       izquierda();
      delay(retardo);
    }



    
   for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }

}

void dibujarF() {
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
    }
   for (int i = 0; i < altura; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       derecha();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       izquierda();
      delay(retardo);
    }
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
    }
    for (int i = 0; i < anchura/2; i++) {
       derecha();
      delay(retardo);
    }
    for (int i = 0; i < anchura/2; i++) {
       izquierda();
      delay(retardo);
    }

}

void dibujarU() {
  digitalWrite(laserpin, LOW);
   for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anchura/2; i++) {
       derecha();
      delay(retardo);
    }


    
  digitalWrite(laserpin, HIGH);
   for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       izquierda();
      delay(retardo);
    }
   for (int i = 0; i < altura; i++) {
      subir();
      delay(retardo);
    }




  digitalWrite(laserpin, LOW);
   for (int i = 0; i < altura/2; i++) {
      bajar();
      delay(retardo);
    }
    for (int i = 0; i < anchura/2; i++) {
       derecha();
      delay(retardo);
    }


  digitalWrite(laserpin, HIGH);

    

}

void dibujarE2() {
   for (int i = 0; i < anchura; i++) {
      derecha();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       izquierda();
      delay(retardo);
    }


    
  digitalWrite(laserpin, LOW);
   for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       derecha();
      delay(retardo);
    }
    digitalWrite(laserpin, HIGH);
    
   for (int i = 0; i < anchura; i++) {
      izquierda();
      delay(retardo);
    }
     for (int i = 0; i < altura; i++) {
      bajar();
      delay(retardo);
    }
     for (int i = 0; i < anchura; i++) {
      derecha();
      delay(retardo);
    }
    

  digitalWrite(laserpin, LOW);
   for (int i = 0; i < altura/2; i++) {
      subir();
      delay(retardo);
    }
    for (int i = 0; i < anchura; i++) {
       izquierda();
      delay(retardo);
    }
  digitalWrite(laserpin, HIGH);

    

}

int t = 5;

void loop() {
 digitalWrite(laserpin, HIGH);
for(int i=0;i<t;i++)dibujarBarra();
for(int i=0;i<t;i++)dibujarH();
for(int i=0;i<t;i++)dibujarCuadrado();
for(int i=0;i<t;i++)dibujarM();
for(int i=0;i<t;i++)dibujarE2();
for(int i=0;i<t;i++)dibujarBarra();
for(int i=0;i<t;i++)dibujarJ();
for(int i=0;i<t;i++)dibujarP();
for(int i=0;i<t;i++)dibujarG();

for(int i=0;i<3*t;i++)dibujarLOGO();
digitalWrite(laserpin, LOW);
delay(1000);
digitalWrite(laserpin, HIGH);
for(int i=0;i<t;i++)dibujarF();
for(int i=0;i<t;i++)dibujarI2();
for(int i=0;i<t;i++)dibujarU();
for(int i=0;i<t;i++)dibujarM();
digitalWrite(laserpin, LOW);
delay(1000);
}
